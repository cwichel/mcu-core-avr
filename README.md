## MCU-Libraries
This repository contains a basic library to the following MCU:
- AVR8
  - ATmega16U4, ATmega32U4
  - ATmega168, ATmega328

The documentation can be browsed [here](https://cwichel.gitlab.io/mcu-core-avr).
