/**
 * @file 	libs/utils/callbacks.cpp
 * @brief 	Callback definitions.
 */
//###############################################
#include <utils/base.h>
//###############################################
void fun_vd_vd_dummy(void) {

}

void fun_vd_u8_dummy(uint8_t value) {

}

uint8_t fun_u8_u8_dummy(uint8_t value) {
	return 0x00;
}
